<?php

include('config.php');

require 'vendor/autoload.php';
	require 'src/Parse/ParseClient.php';
	require 'src/Parse/ParseObject.php';
	use Parse\ParseClient;
	use Parse\ParseObject;
	use Parse\ParseQuery;
ParseClient::initialize($parse_app_key, $parse_rest_key, $parse_master_key);

$key = null;
if(isset($_GET['key'])) {
	$key = $_GET['key'];
} 

$table = null;
if(isset($_GET['table'])) {
	$table = $_GET['table'];
} 


header('Content-Type: application/excel');
header('Content-Disposition: attachment; filename="contacts.csv"');

$fp = fopen('php://output', 'w');

$parse = new parseQuery($table);
if ($key != NULL) {
$parse->equalTo('Key', $key); 
$parse->limit(1000);
}
$result = $parse->find(); 
$printed_header = false;
error_log("size: " + count($result));

$header_arr = array(
"Key",
"Email",
"Name"
);

fputcsv($fp, $header_arr);

error_log("number of results: " + count($result));

function getWithoutCase($val, $header_field) {
    $cur_val = $val->get(ucfirst ($header_field));
    if ($cur_val == NULL) {
        $cur_val = $val->get(lcfirst ($header_field));
    }
    return $cur_val;
}

foreach($result as $title=>$val) {
    $data = array();
    foreach ($header_arr as $header_field) {
        $cur_val = getWithoutCase($val, $header_field);
        if ($cur_val != NULL) {
            array_push($data, $cur_val);
        } else {
            array_push($data, "");
        }
            if (strtolower($header_field) === "email") {
                array_push($data, getWithoutCase($val, "email"));
            }
    }
    if (!$printed_header) {
        $printed_header = true;
    }
    fputcsv($fp, $data);
}
							
fclose($fp);

?>