<?php


class petFinder {

	const API_KEY        = 'b30318140b00865c7eec830fb7443b5d';
	const API_SECRET     = 'c7c1748f395cdb59e1e566f46e4f5b90';
	const API_URL        = 'http://api.petfinder.com/';
	public $sesionToken  = '';
	public $searchParams = array();

	function __construct()
	{
		$this->searchParams['location'] = '';
		$this->searchParams['breed'] = '';
		$this->searchParams['size'] = '';
		$this->searchParams['sex'] = '';
		$this->searchParams['age'] = '';
	}

	// Get inputs from user
	public function getInputs()
	{
		echo "\n\nEnter Search options (*mendatory) : \n\n";
		echo "*Location (Zip/Postal code/City/State): ";
		$this->searchParams['location'] = trim(fgets(STDIN));

		// Validate location
		while($this->searchParams['location'] == '')
		{
			echo "\nLocation should not be blank\n";
			echo "*Location : ";
			$this->searchParams['location'] = trim(fgets(STDIN));
		}


		echo "Animal (barnyard, bird, cat, dog, horse, pig, reptile, smallfurry) : ";
		$this->searchParams['animal'] = trim(fgets(STDIN));

		echo "Breed : ";
		$this->searchParams['breed'] = trim(fgets(STDIN));

		echo "Size (S=small, M=medium, L=large, XL=extra-large) : ";
		$this->searchParams['size'] = trim(fgets(STDIN));

		// Validate size
		while(($this->searchParams['size'] != '') && ($this->searchParams['size'] != 'S' && $this->searchParams['size'] != 'M' && $this->searchParams['size'] != 'L' && $this->searchParams['size'] != 'XL'))
		{
			echo "\nEnter valid size\n";
			echo "Size (S=small, M=medium, L=large, XL=extra-large) : ";
			$this->searchParams['size'] = trim(fgets(STDIN));
		}
		
		echo "Sex (M=male, F=female) : ";
		$this->searchParams['sex'] = trim(fgets(STDIN));

		// Validate sex
		while(($this->searchParams['sex'] != '') && ($this->searchParams['sex'] != 'M' && $this->searchParams['sex'] != 'F'))
		{
			echo "\nEnter valid sex\n";
			echo "Sex (M=male, F=female) : ";
			$this->searchParams['sex'] = trim(fgets(STDIN));
		}

		echo "Age (Baby, Young, Adult, Senior) : ";
		$this->searchParams['age'] = ucwords(trim(fgets(STDIN)));

		// Validate age
		while(($this->searchParams['age'] != '') && ($this->searchParams['age'] != 'Baby' && $this->searchParams['age'] != 'Young' && $this->searchParams['age'] != 'Adult' && $this->searchParams['age'] != 'Senior'))
		{
			echo "\nEnter valid age\n";
			echo "Age (Baby, Young, Adult, Senior) : ";
			$this->searchParams['age'] = ucwords(trim(fgets(STDIN)));
		}
		
	}


	// Connect with api
	public function connect()
	{
		$sign            = MD5(self::API_SECRET.'key='.self::API_KEY);
		$sessionTokenUrl = self::API_URL.'auth.getToken?key='.self::API_KEY.'&sig='.$sign;

		$output = $this->curlExec($sessionTokenUrl);

		//Convert to json
        $xml = simplexml_load_string($output);
        if ($xml === false) {
		    echo "Failed loading XML: ";
		    foreach(libxml_get_errors() as $error) {
		        echo "<br>", $error->message;
		    }
		    exit;
		} else {
		    $json         = json_encode($xml);
		    $arrResult    = json_decode($json,TRUE);
		    $this->sessionToken = $arrResult['auth']['token'];
		}

        // close curl resource to free up system resources 
        //curl_close($ch); 

        //return $sessionToken;

	}

	public function search($params = array())
	{
		//$this->getInputs();
		$findUrl = self::API_URL.'pet.find?key='.self::API_KEY.'&token='.$this->sessionToken.'&format=json&';
		$searchParam = array();

		if(count($params) > 0)
		{
			$this->searchParams = $params;
		}


		foreach($this->searchParams as $key=>$param)
		{
			if(trim($param) != '')
			{
				$searchParam[] = $key.'='.$param;
			}
		}

		$query = implode('&', $searchParam);		


		$result = $this->curlExec($findUrl.$query);

		echo "\n\n\nResult : \n\n\n";

		print_r($result);
	}

	public function curlExec($url)
	{
		$ch = curl_init(); 

        // set url 
        curl_setopt($ch, CURLOPT_URL, $url); 

        //return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

        // $output contains the output string 
        $output = curl_exec($ch);

        return $output;
	}
}



?>